package game

import java.net.Socket
import kotlin.concurrent.thread

class GameServer(
  private val out: (Any?) -> Unit = { x -> println(x) }
)
{
  private val server: Server = Server(4, out)
  val port: Int = server.port
  private val msgMap: MutableMap<String, (s: Socket) -> Unit> = mutableMapOf()
  private var running: Boolean = true
  private var gameState = GameState(0, mutableMapOf())

  init
  {
    msgMap["exit"] = this::shutDown
    msgMap["shutdown"] = this::shutDown
    msgMap["hello_world"] = this::helloWorld
    this.out("Initialized with MsgMap: $msgMap")
  }

  private fun shutDown(ignore: Socket)
  {
    server.close()
    running = false
    out("${Thread.currentThread().threadId()}: Shut down game server")
  }

  fun gameStart()
  {
    thread { server.open() }
    while (running)
    {
      evalNextCommand()
    }
  }

  private fun helloWorld(ignore: Socket)
  {
    out("${Thread.currentThread().threadId()}: Hello World worked")
  }

  private fun evalNextCommand()
  {
    val msg: Message = server.buffer.pull()
    out("${Thread.currentThread().threadId()}: Evaluating command: $msg")
    if (msg.sender == null)  // eager pulling in the beginning of the game
    {
      return
    }

    if (this.msgMap.containsKey(msg.command))
    {
      this.out("${Thread.currentThread().threadId()}: Invoking ${msg.command}")
      this.msgMap[msg.command]?.invoke(msg.sender ?: return)
    }
    else
    {
      this.out(
        "${
          Thread.currentThread().threadId()
        }: received unknown message ${msg.command} from ${msg.sender} for map $msgMap"
      )
    }
  }
}