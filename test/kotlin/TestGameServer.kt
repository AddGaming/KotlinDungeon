import game.Client
import game.GameServer
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import test_util.OutBuffer
import kotlin.concurrent.thread

class TestGameServer
{
  @Test
  fun test_server_accepts_newly_defined_fns_in_map()
  {
    val serverOut = OutBuffer()
    val testServer = GameServer(
      out = { x -> serverOut.write(x) },
    )
    val serverThread = thread {
      testServer.gameStart()
      while (!serverOut.get().last().contains("Shut down game server"))
      {
        // block?
      }
    }
    thread {
      val client = Client(port = testServer.port, out = { x -> serverOut.write(x) })
      client.send("hello_world")
      client.send("shutdown")
    }
    serverThread.join()
    Assertions.assertTrue(serverOut.getStr().contains("Hello World worked"))
  }
}