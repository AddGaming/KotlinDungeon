package game

import java.net.Socket
import java.util.*
import kotlin.concurrent.thread

class Client(
  hostIp: String = "127.0.0.1",
  port: Int = 5050,
  private val out: (Any?) -> Unit = { x -> println(x) }
)
{
  private val socket: Socket = Socket(hostIp, port)
  private var running: Boolean = true

  init
  {
    thread { receive() }
  }

  fun send(rawMsg: String)
  {
    val msg = if (rawMsg.endsWith("\n")) rawMsg else rawMsg.plus("\n")
    this.out("${Thread.currentThread().threadId()}: sent '$msg'")
    this.socket.getOutputStream().write(msg.toByteArray())
  }

  private fun receive()
  {
    val scanner = Scanner(socket.getInputStream())
    this.out("${Thread.currentThread().threadId()}: client $this is ready to receive messages")
    while (this.running)
    {
      try
      {
        val text = scanner.nextLine()
        this.out("${Thread.currentThread().threadId()}: received msg '$text' from server")
      }
      catch (_: NoSuchElementException)  // caused by server closed
      {
      }
    }
  }

  fun close()
  {
    running = false
    socket.close()
  }
}