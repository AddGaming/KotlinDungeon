package game

class Room(
  val block: Int,
  val damage: Int,
)
{
  val neighbours: Array<Room?> = Array<Room?>(4) { _ -> null }
}
