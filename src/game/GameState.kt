package game

data class GameState(
  val round: Int,
  val players: MutableMap<String, Player>
)
