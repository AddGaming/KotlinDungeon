package game

import java.net.Socket

/**
 * Sender: the one who sent the message
 * command: the first arg of the received message aka the command
 * args: a list of arguments accompanying the command
 * TODO: How do I make this thing live on the heap? L1 utilization in ring buffer would be nice
 */
data class Message(
  var sender: Socket?,
  var command: String?,
  val args: Array<String> = Array(10) { "" } // I suspect I don't need more args than 10
)
{
  override fun equals(other: Any?): Boolean
  {
    if (this === other) return true
    if (javaClass != other?.javaClass) return false

    other as Message

    if (sender != other.sender) return false
    if (command != other.command) return false
    return args.contentEquals(other.args)
  }

  override fun hashCode(): Int
  {
    var result = sender.hashCode()
    result = 31 * result + command.hashCode()  // remember to always choose primes kids!
    result = 37 * result + args.contentHashCode()
    return result
  }
}
