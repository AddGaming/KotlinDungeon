package test_util

/**
 * A test util that saves all output received in a giant list
 */
class OutBuffer
{
  private val buffer: MutableList<String> = mutableListOf()

  fun write(input: Any?)
  {
    when (input)
    {
      is String -> buffer.add(input)
      null ->
      {
      }

      else -> buffer.add(input.javaClass.simpleName)
    }
  }

  fun get(): List<String>
  {
    return buffer.toList()
  }

  fun getStr(): String
  {
    return buffer.fold("", String::plus)
  }

  @Suppress("USELESS_CAST")
  fun countMatch(str: String): Int
  {
    return buffer
      .map { s -> s.contains(str) }
      .sumOf { x ->
        if (x) 1 as Int else 0
      } // cast needed for overloading!
  }
}