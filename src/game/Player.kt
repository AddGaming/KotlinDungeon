package game

data class Player(
  val name: String,
  val gold: Int,
  val rooms: List<Room>
)
