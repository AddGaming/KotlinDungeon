package game

/**
 * This ring buffer implementation will return a null
 * if the ring buffer is full
 * To use this threat save, throw away input that does not fit.
 * !DON'T! Try to hold onto it until space is free.
 * https://lmax-exchange.github.io/disruptor/disruptor.html
 * https://lmax-exchange.github.io/disruptor/user-guide/index.html
 * https://github.com/LMAX-Exchange/disruptor/wiki/Blogs-And-Articles
 */
class OptionalRingBuffer<T>(
  private val buffer: Array<T>,
)
{

  private var modifyPointer: Int = 0
  private var committedPointer: Int = 0
  private var readPointer: Int = buffer.size - 2
  private val lock = Object()

  fun pull(): T
  {
    /* println(
      "${
        Thread.currentThread().threadId()
      }: buffer <$modifyPointer $committedPointer $readPointer>: "
          + buffer.map { msg -> "$msg" }.toList().joinToString(",")
    ) */
    while (readPointer == committedPointer)
    {
      synchronized(lock)
      {
        /* println(
          "${
            Thread.currentThread().threadId()
          }: trying to pull from empty -> sleeping ${Thread.currentThread().threadId()}"
        )*/
        lock.wait()
      }
    }
    readPointer = (readPointer + 1) % buffer.size
    return buffer[readPointer]
  }

  fun modify(): Pair<T, Int>?
  {
    // TODO: test the element accessibility
    return if ((modifyPointer + 1) % buffer.size == readPointer)
    {
      null
    }
    else
    {
      modifyPointer = (modifyPointer + 1) % buffer.size
      Pair(buffer[modifyPointer], modifyPointer)  // returns obj reference
    }
  }

  /**
   * threat pausing means that client messages will not be received while spinning.
   * I could fix that with tcp, but this is already overkill...
   */
  fun commit(index: Int)
  {
    // TODO: test correct threat paused and un-paused
    while (committedPointer + 1 != index)
    {
      synchronized(lock)
      {
        /* println(
          "${
            Thread.currentThread().threadId()
          }: trying to commit while previous task not done yet -> sleeping ${
            Thread.currentThread().threadId()
          }"
        ) */
        lock.wait()
      }
    }
    committedPointer = (committedPointer + 1) % buffer.size
    synchronized(lock)
    {
      /* println(
        "${
          Thread.currentThread().threadId()
        }: new insert <${buffer[committedPointer]}> @ $committedPointer approved -> waking from ${
          Thread.currentThread().threadId()
        }"
      ) */
      lock.notifyAll()
    }
  }
}

