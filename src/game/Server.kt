package game

import java.net.*
import java.util.*
import kotlin.concurrent.thread

/**
 * A generic server that wraps sockets and can send and receive messages
 * A server can't be restarted once closed -> create a new one
 * Usage:
 * - open with `open`
 * - `send` or `broadcast` your messages
 * - access incoming messages over the `buffer`
 * - `close` the server once you are done
 */
class Server(
  private var maxConnections: Int,
  private val out: (Any?) -> Unit = { x -> println(x) },
)
{
  val port: Int = claimPort()
  val buffer: OptionalRingBuffer<Message> =  // make this private when u know how it is used
    OptionalRingBuffer(Array(10 * maxConnections) { _ -> Message(null, null) })
  private val socket: ServerSocket = ServerSocket(port)
  private val connections: MutableList<Socket> = LinkedList<Socket>()
  private var running: Boolean = true

  private fun claimPort(): Int
  {
    var wish = 5050
    while (true)
    {
      try
      {
        val s = ServerSocket(wish)
        s.close()
        return wish
      }
      catch (_: BindException)
      {
        wish += 1
      }
    }
  }

  fun open()
  {
    this.running = true
    this.out("${Thread.currentThread().threadId()}: Running Server")
    while (this.running && this.connections.size < this.maxConnections)
    {
      try
      {
        this.out(
          "${
            Thread.currentThread().threadId()
          }: waiting for connection ${this.connections.size}"
        )
        val accepted = this.socket.accept()  // this freezes' time
        if (!running) continue
        this.out("${Thread.currentThread().threadId()}: got connection")
        this.connections.add(accepted)
        thread { this.receive(accepted) }
      }
      catch (_: SocketException)
      {
      }
    }
  }

  /**
   * Changes the amount of allowed connections to be the current number of connections.
   * Hence, closing to server for new connection attempts
   */
  fun setFull()
  {
    this.maxConnections = this.connections.size
  }

  fun broadcast(rawMsg: String)
  {
    val msg = if (rawMsg.endsWith("\n")) rawMsg else rawMsg.plus("\n")
    for (s in this.connections)
    {
      this.out("${Thread.currentThread().threadId()}: broadcasting '$msg' to $s")
      s.getOutputStream().write(msg.toByteArray())
    }
  }

  fun send(rawMsg: String, receiver: Socket)
  {
    val msg = if (rawMsg.endsWith("\n")) rawMsg else rawMsg.plus("\n")
    this.out("${Thread.currentThread().threadId()}: broadcasting '$msg' to $receiver")
    receiver.getOutputStream().write(msg.toByteArray())
  }

  private fun receive(client: Socket)
  {
    val scanner = Scanner(client.getInputStream())
    this.out("${Thread.currentThread().threadId()}: ready to receive messages from $client")
    while (this.running)
    {
      try
      {
        val text = scanner.nextLine()
        this.out("${Thread.currentThread().threadId()}: received msg '$text' from $client")
        val (msg: Message?, index: Int) = this.buffer.modify() ?: (null to 0)
        if (msg == null)  // ring buffer is full
        {
          this.send("overload", client)
        }
        else
        {
          val split = text.split(" ")
          msg.command = split[0]
          msg.sender = client
          for (i in 0 until msg.args.size)
          {
            try
            {
              msg.args[i] = split[i + 1]
            }
            catch (e: IndexOutOfBoundsException)
            {
              msg.args[i] = ""
            }
          }
          this.buffer.commit(index)
        }
      }
      catch (_: NoSuchElementException)  // caused by server closed
      {
      }
    }
  }

  fun close()
  {
    this.running = false
    val iterator = this.connections.iterator()
    while (iterator.hasNext())
    {
      val s = iterator.next()
      this.out("${Thread.currentThread().threadId()}: closing connection $s")
      s.close()
      iterator.remove()
    }
    socket.close()
    this.connections.clear()
  }
}
