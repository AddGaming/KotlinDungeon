import game.Client
import game.Server
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import test_util.OutBuffer
import kotlin.concurrent.thread

class TestServer
{

  @Test
  fun test_accepts_one_connections()
  {
    val outBuffer = OutBuffer()
    val testServer = Server(
      maxConnections = 1,
      out = { x -> outBuffer.write(x) }
    )
    val serverThread = thread {
      testServer.open()
      testServer.close()
    }
    val clientThread = thread { Client(port = testServer.port) }
    clientThread.join()
    serverThread.join()
    assertTrue(outBuffer.getStr().contains("connection 0"))
    assertTrue(outBuffer.getStr().contains("closing"))
  }

  @Test
  fun test_accepts_multiple_connections()
  {
    val outBuffer = OutBuffer()
    val testServer = Server(
      maxConnections = 3,
      out = { x -> outBuffer.write(x) }
    )
    val serverThread = thread {
      testServer.open()
      testServer.close()
    }
    thread { Client(port = testServer.port) }
    thread { Client(port = testServer.port) }
    thread { Client(port = testServer.port) }
    serverThread.join()
    assertEquals(3, outBuffer.countMatch("waiting for connection"))
    assertEquals(3, outBuffer.countMatch("closing"))
  }

  @Test
  fun test_server_accepts_premature_exiting()
  {
    val serverOut = OutBuffer()
    val testServer = Server(
      maxConnections = 3,
      out = { x -> serverOut.write(x) }
    )
    val serverThread = thread {
      testServer.open()
    }
    thread { Client(port = testServer.port) }
    thread {
      Client(port = testServer.port)
      testServer.close()
    }
    serverThread.join()
    assertEquals(
      serverOut.countMatch("got connection"),
      serverOut.countMatch("closing connection")
    )
  }

  @Test
  fun test_broadcast_broadcasts_to_all_clients()
  {
    val serverOut = OutBuffer()
    val clientOut = OutBuffer()
    val testServer = Server(
      maxConnections = 2,
      out = { x -> serverOut.write(x) }
    )
    val serverThread = thread {
      testServer.open()
      testServer.broadcast("TestString\n")
      Thread.sleep(10)  // messages take time to arrive
    }
    thread { Client(port = testServer.port, out = { x -> clientOut.write(x) }) }
    thread { Client(port = testServer.port, out = { x -> clientOut.write(x) }) }
    serverThread.join()
    assertEquals(2, clientOut.countMatch("'TestString'"))
    assertEquals(2, clientOut.countMatch("received msg"))
  }

  @Test
  fun test_broadcast_adds_newline_when_missing_on_message()
  {
    val serverOut = OutBuffer()
    val clientOut = OutBuffer()
    val testServer = Server(
      maxConnections = 2,
      out = { x -> serverOut.write(x) }
    )
    val serverThread = thread {
      testServer.open()
      testServer.broadcast("TestString")
      Thread.sleep(10)  // messages take time to arrive
    }
    thread { Client(port = testServer.port, out = { x -> clientOut.write(x) }) }
    thread { Client(port = testServer.port, out = { x -> clientOut.write(x) }) }
    serverThread.join()
    assertEquals(2, clientOut.countMatch("TestString"))
    assertEquals(2, clientOut.countMatch("received msg"))
  }

  @Test
  fun test_setting_server_to_full_changes_max_allowed_connections()
  {
    val serverOut = OutBuffer()
    val clientOut = OutBuffer()
    val testServer = Server(
      maxConnections = 5,
      out = { x -> serverOut.write(x) }
    )
    val serverThread = thread {
      testServer.open()
    }
    thread { Client(port = testServer.port, out = { x -> clientOut.write(x) }) }
    thread {
      Client(port = testServer.port, out = { x -> clientOut.write(x) })
      testServer.setFull()
    }
    serverThread.join()
    testServer.close()
  }
}