# Communication Protocol

The communication protocol is rather simple.

- Communication happens via strings
- each message ends with a newline
- each parameter gets separated with a "` `" (space)

That's it.

## Game specific messages

There are two kinds of communication: Events and Commands.
Commands are things the client sends to the server.
Events are the reverse; things the server sends to the client.
So make sure your client can handle all events.
Meanwhile, the server should handle all commands.

### Commands

| syntax                 | explanation                          | example                |
|------------------------|--------------------------------------|------------------------|
| `exit`                 | TODO: player leaves the game         | `exit`                 |
| `shutdown`             | shuts the server down                | `shutdown`             |
| `build <room> <x> <y>` | builds a new room at the location    | `build spike_trap 1 1` |
| `upgrade <x> <y>`      | upgrades a existing room at location | `upgrade 1 1`          |
| `start_combat`         | starts the combat phase              | `start_combat`         |

### Events

| syntax                   | explanation                                              | example                         |
|--------------------------|----------------------------------------------------------|---------------------------------|
| `player_join <name>`     | player joins the game                                    | `player_join felix`             |
| `overload`               | the server is currently under huge load, try again later | `overload`                      |
| `invalid <msg>`          | the command send was invalid                             | `invalid cant build in combad ` |
| `success <msg>`          | the command send was executed                            | `success room build`            |
| `combat_started`         | notification that the combat phase has started           | `combat_started`                |
| `preparation_started`    | notification that the preparation phase has started      | `preparation_started`           |
| `received_gold <amount>` | player gold increased                                    | `received_gold 5`               |

# The Game

So what is this game and how is it played?
Each player has a **Dungeon**.
This dungeon consists of different rooms.
The rooms have to be connected to the final room, the **Dungeon Core**.
Each round is split into two parts.
The preparation phase where the player can place rooms and improve existing ones,
and a battle phase where enemies will try to conquer your dungeon.
If enemies reach your dungeon core, you will lose health equal to the strength of the enemy.
If your health reaches 0, you are out.

## Rooms

Each Room has a cost associated with them.
If you have enough **Gold**, you can by yourself a room.
You can upgrade already existing rooms with gold too.
The enemies will always spawn in the room that is the furthest away from the core.
Rooms are only active when they are connected to your core to receive energy.

### List of Rooms

| name         | block | damage |
|--------------|-------|--------|
| `Goblin Den` | 1/2/3 | 1/2/3  |
| `Spike Trap` | 0     | 5/7/10 |

## Enemies

| stat             | description                                                                             |
|------------------|-----------------------------------------------------------------------------------------|
| `hp`             | health points                                                                           |
| `block required` | what amount of `block` is required for this unit to be stopped                          |
| `speed`          | the amount of turns the unit will spend inside a single room (high speed stat is slower |
| `core dmg`       | the damage this unit will do when it reaches the core                                   |
| `gold`           | the amount of gold that drops when this enemy is defeated                               |

Enemies will always walk the shortest path to the core.
